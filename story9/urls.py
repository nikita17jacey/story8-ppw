from django.contrib import admin
from django.urls import path, include
from .views import Index, search, top5_url, addLike_url

app_name = 'story9'

urlpatterns = [
    path('', Index, name= 'Index'),
    path('search', search, name='books_data'),
    path('addLike_url', addLike_url, name = 'addLike_url'),
    path('top5_url', top5_url, name='top5_url')
]