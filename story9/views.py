from django.shortcuts import render
from django.http import JsonResponse
import json
import requests
from .models import Book
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def Index(request):
    return render(request,'story9.html')

def search(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"

    json_response= request.get("https://www.googleapis.com/books/v1/volumes?q=" + q).json()
    for i in json_response['items']:
        try:
            data_book = Book.objects.get(id=i['id'])
            like = data_book.like
        except:
            like = 0
        i['volumeInfo']['like'] = like

    return JsonResponse(json_response)

@csrf_exempt
def addLike_url(request):
    data_book, created = Book.objects.get_or_create(
        id=request.POST['id'],
        cover=request.POST['cover'],
        title=request.POST['title'],
        author=request.POST['author'],
        like=request.POST['like']
    )
    data_book.save()
    return JsonResponse(data_book.like, safe=False)

def top5_url(request):
    data_book_sort= Book.objects.order_by('-like')[:5]
    top5_list=[]
    for i in data_book_sort:
        top5_list.append({
            'id' : i.id,
            'cover': i.cover,
            'title': i.title,
            'author':i.author,
            'like': i.like
        })
    data={
        'top5_list' : top5_list,
    }
    return JsonResponse(data,safe=False)



