from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

import time
import unittest
# Create your tests here.

class Stroy9FuncTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.maximize_window() #For maximizing window
        self.browser.implicitly_wait(20)
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_landing_page(self):
        return (self.live_server_url)
        self.browser.get(self.live_server_url + '/story9/')
        self.assertIn('BOOOOOKS', self.browser.title)
        self.assertIn('SEARCH', self.browser.page_source)
        time.sleep(3)

class Story9UnitTest(TestCase):
    def test_page(self):
        response = Client().get("/story9/")
        self.assertEqual(response.status_code,200)

    def test_page_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response,'story9.html')

