from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

import time
import unittest
# Create your tests here.

class Stroy8FuncTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.maximize_window() #For maximizing window
        self.browser.implicitly_wait(20)
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_landing_page(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn('Hello', self.browser.title)
        self.assertIn('U: Up', self.browser.page_source)
        time.sleep(3)

    def test_display(self):
        self.browser.get(self.live_server_url + '/')
        about = self.browser.find_element_by_css_selector('li:nth-child(1) > .accordion')
        exp = self.browser.find_element_by_css_selector('li:nth-child(2) > .accordion')

        about.click()
        exp.click()


        self.assertIn('Hello', self.browser.page_source)
        self.assertIn('DNC', self.browser.page_source)


    def test_up_down_button(self):
        self.browser.get(self.live_server_url + '/')
        up_exp = self.browser.find_element_by_css_selector('li:nth-child(2) > .myButton:nth-child(1)')
        down_contact = self.browser.find_element_by_css_selector('li:nth-child(3) > .myButton:nth-child(2)')

        up_exp.click()

        down_contact.click()








