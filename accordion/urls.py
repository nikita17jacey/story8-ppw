from django.contrib import admin
from django.urls import path, include
from .views import Index

app_name = 'message'

urlpatterns = [
    path('', Index, name= 'Index'),
]